﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecretNumber
{
    internal class Program
    /// <summary>
    /// Auteur : Henry VCDRN
    /// Projet : Jeux Casino 
    /// Details : Amelioration Jeux "Secret Number" / Ajout de la mise en page et finalisation du jeux.
    /// Date de Creation : 18 / 11 / 2022
    /// Version : V3
    /// </summary>
    {
        static void Main(string[] args)
        {
            SecretNumberGame();
            Console.ReadKey();
        }
        static void SecretNumberGame()
        {
            DisplayLine();
            Console.WriteLine("Bienvenu au jeux Secret Number ! ");
            Console.WriteLine("Les régles sont simples ! Vous devez trouver un nombre entre 0 et 100.");
            Console.WriteLine("Voici les conditions de gain :");
            Console.WriteLine("Si vous trouvez le nombre au 1er essais vous multipliez votre mise * 5 !");
            Console.WriteLine("Si vous trouvez le numero entre 2 et 5 essais vous multipliez votre mise * 3 !");
            Console.WriteLine("Si vous trouvez le numero entre 6 et 10 essais vous multpliez votre mise * 2 !");
            Console.WriteLine("Vous avez 10 Oportunités de multiplier votre argent, Bonne chance !");
            DisplayLine();
            Random rand = new Random();
            int nbsecret;
            nbsecret = rand.Next(101);
            int i = 1;
            Console.WriteLine("Combien voulez vous miser ?");
            Console.Write("Mettez votre mise ici : ");
            int mise = Convert.ToInt32(Console.ReadLine());

            while (i <= 10)
            {
                DisplayOtherLine();
                Console.WriteLine($"Opportunité numméro {i}");
                int nb = Convert.ToInt32(Console.ReadLine());
                DisplayOtherLine();
                Console.WriteLine();
                if (nb == nbsecret)
                {
                    if (i == 1)
                    {
                        DisplayLine();
                        Console.WriteLine($"Bravo ! Vous avez trouvé le numéro secret en {i} essais ! Vous avez multiplié votre mise * 5");
                        Console.WriteLine($"Vous avez gagné {mise * 5} Francs !");
                        DisplayLine();
                    }
                    else if (i <= 5)
                    {
                        DisplayLine();
                        Console.WriteLine($"Bravo ! Vous avez trouvé le numéro secret en {i} essais ! Vous avez multiplié votre mise * 3");
                        Console.WriteLine($"Vous avez gagné {mise * 3} Francs !");
                        DisplayLine();
                    }
                    else if (i >= 6)
                    {
                        DisplayLine();
                        Console.WriteLine($"Bravo ! Vous avez trouvé le numéro secret en {i} essais ! Vous avez multiplié votre mise * 2");
                        Console.WriteLine($"Vous avez gagné {mise * 2} Francs ");
                        DisplayLine();
                    }
                    return;
                }
                if (nb < nbsecret)
                {
                    DisplayLine();
                    Console.WriteLine("Le numéro secret est plus grand ! Essayez a nouveau :D ");
                    DisplayLine();
                    Console.WriteLine();
                }
                else if (nb > nbsecret)
                {
                    DisplayLine();
                    Console.WriteLine("Le numéro secret est plus petit ! Essayez a nouveau :D ");
                    DisplayLine();
                    Console.WriteLine();
                }
                i++;
            }
            if (i != 10)
            {
                DisplayLine();
                Console.WriteLine($"Vous avez misé {mise} Francs");
                Console.WriteLine($"Vous avez perdu {mise} Francs !");
                DisplayLine();
            }
        }
        static void DisplayLine()
        {
            Console.WriteLine("==========================================================================================================");
        }
        static void DisplayOtherLine()
        {
            Console.WriteLine("************************************************************************************************************");
        }
    }
}
