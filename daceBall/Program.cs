﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Lifetime;
using System.Text;
using System.Threading.Tasks;
/*
 * Projet : faire un casino
 * Détails : jeu : DiceBall
 * Auteur : Maya Gerber
 * Date : 11.11.2022
 * version : v1
 */
namespace diceBall
{
    internal class Program
    {
        /// <summary>
        /// En lançant les dés, le lanceur devient maître du jeu et tente d’en déterminer l’issue. 
        /// Le but est de réaliser le plus de runs possibles. 
        /// Un run dure tant que le lanceur n’a pas fait de 7. Chaque fois qu’un 7 sort, on passe à un autre lanceur et le run est terminé. 
        /// Plus il y a eu de lancers sans 7, plus rapporte la mise.
        /// Après chaque lancé, vous pouvez choisir si vous rester ou partez. 
        /// Chaque tour où la somme des dés n'est pas 7 et que vous êtes encore dedans, alors votre mise augmentra de 10%.
        /// Si vous êtes encore dans la partie et que la somme des dés est égal à 7 alors vous perdez la mise que vous avez misé.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Console.WriteLine("--------------------------");
            Console.WriteLine("Bienvenue au diceball");
            Console.WriteLine("--------------------------");
            Console.WriteLine("Règles du jeu");
            Console.WriteLine("..............");
            regles();
            Console.Write("Entrez votre mise : ");
            int mise = Convert.ToInt32(Console.ReadLine());
            Random rand = new Random();
            int nbrandom1 = rand.Next(1, 7);
            int nbrandom2 = rand.Next(1, 7);
            int i = 1;
            int resultat = nbrandom1 + nbrandom2;
           
            while (resultat != 7)
            {
                nbrandom1 = rand.Next(1, 7);
                nbrandom2 = rand.Next(1, 7);
                resultat = nbrandom1 + nbrandom2;

                Console.WriteLine($"{i})");
                Console.WriteLine($"Dé n°1 : {nbrandom1}");
                Console.WriteLine($"Dé n°2 : {nbrandom2}");
                Console.WriteLine($"La somme est {resultat}");
                if (resultat == 7)
                {
                    Console.WriteLine("Le jeu est terminé...");
                    Console.WriteLine($"Vous avez perdu {mise} CHF");
                    return;
                }
                
                Console.WriteLine("Veux-tu rester ou sortir ? (rester/sortir)");
                string resterSortir = Console.ReadLine();
                if (resterSortir == "sortir")
                {
                    
                    int benefice = (mise * 5 / 100) * i;
                    int miseFinale = benefice + mise;
                    Console.WriteLine($"Félicitation ! Vous avez gagner {benefice} CHF");
                    Console.WriteLine($"Vous avez en tout {miseFinale} CHF");
                    return;
                }
                

                i++;
            }

        }static void regles()
        {
            Console.WriteLine("Deux dés de 6 sont aléatoirement lancé chaque tour.");
            Console.WriteLine("Vous gagnez +5% de votre mise chaque tour tant que la somme des dés n'est pas égale à 7.");
            Console.WriteLine("Si la somme des dés est égale à 7 alors la partie s'arrête et vous perdez votre mise.");
            Console.WriteLine("Après chaque tour, vous avez la possibilité de rester ou de sortir de la partie.");
            Console.WriteLine("Si vous décidez de partir avant que la somme ne soit 7 alors vous gagnez pour chaque tour resté +5% de votre mise.");
            Console.WriteLine("Si vous sortez pas à temps, vous perdez votre mise.");
            Console.WriteLine("-------------------------");
            
            
        }
    }
}
