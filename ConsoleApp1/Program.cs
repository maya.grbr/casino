﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Auteur : Yoan, Maya et Henry
 * Projet : Menu démarrage
 * Détail : Faire un menu démarrage pour naviguer entre les différants jeux et le réstaurant
 */

namespace main
{
    internal class Program
    {
        static void Main(string[] args)
        {
            messageBienvenue();
            DisplayLine();
            ChoixJeu();
            Console.ReadKey();
        }

        static void messageBienvenue()
        {
            Console.WriteLine("Le Casino vous souhaite la bienvenue !");
            Console.WriteLine("Ici, vous retrouvez différents jeux variés et funs tout en gagnat de l'argent !");
            Console.WriteLine("Nous avons un restaurant où vous retrouverez diverses boissons ainsi que des plats salés et sucrés.");
        }

        static void DemarrageJeu(string chemin)
        {
            Process notepad = new Process();
            notepad.StartInfo.FileName = chemin;
            notepad.StartInfo.Arguments = "";
            notepad.Start();
            Console.ReadLine();
        }

        static void ChoixJeu()
        {
            string choixContinuer = "oui";
            while (choixContinuer == "oui")
            {
                Console.WriteLine("Voici les differents jeux disponibles : \n1. -Dace Ball \n2. -Machine à Sous \n3. -Secret Number \n4. -Roulette \n5. -Bataille \n6. -Black Jack");
                int choixJeu = Convert.ToInt32(Console.ReadLine());

                if (choixJeu == 1)
                {
                    DemarrageJeu("..\\..\\..\\daceBall\\bin\\Debug\\daceBall.exe");
                }
                if (choixJeu == 2)
                {
                    DemarrageJeu("..\\..\\..\\machineASous\\bin\\Debug\\machineASous.exe");
                }
                if (choixJeu == 3)
                {
                    DemarrageJeu("..\\..\\..\\SecretNumber\\bin\\Debug\\SecretNumber.exe");
                }
                if (choixJeu == 4)
                {
                    DemarrageJeu("..\\..\\..\\roulette\\bin\\Debug\\roulette.exe");
                }
                if (choixJeu == 5)
                {
                    DemarrageJeu("..\\..\\..\\Bataille\\bin\\Debug\\Bataille.exe");
                }
                if (choixJeu == 6)
                {
                    DemarrageJeu("..\\..\\..\\BlackJack\\bin\\Debug\\BlackJack.exe");
                }

                Console.WriteLine("Voulez vous continuer à jouer, ou voulez vous peut être allez manger un petit truc?");
                Console.WriteLine("- Continuer à jouer tapez oui, sinon tapez non \n- Si vous voulez aller manger tapez manger");
                choixContinuer = Console.ReadLine();
                
                if (choixContinuer == "manger")
                {
                    DemarrageJeu("..\\..\\..\\restaurant\\bin\\Debug\\restaurant.exe");
                }
                if (choixContinuer == "non")
                {
                    return; 
                }
            }
        }

        static void DisplayLine()
        {
            Console.WriteLine();
            Console.WriteLine("===================================================================================================");
            Console.WriteLine();

        }
    }
}