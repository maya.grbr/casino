﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* 
 *  Auteur : Yoan Bompas
 *  Projet : Faire le jeu de la roulette
 *  Détail : Faire comme le vrai jeu de la roulette dans un casino
 */

namespace roulette
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenue au jeu de la roulette");
            Console.WriteLine("Les régles sont simple miser si le nombre va être paire ou impaire.");
            Console.WriteLine("Si vous avez gagner. Vous resevez le double de votre mise");
            Console.WriteLine("Tapez votre mise :");
            int mise = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Tapez impair et tapez pair: ");
            string choix = Console.ReadLine();
            Random rand = new Random();
            int nbroulette = rand.Next(27);

            Console.WriteLine($"Le nombre de la roulette est : {nbroulette}");
            if (choix == "pair" || choix == "paire")
            {
                if (nbroulette % 2 == 0)
                {
                    Console.WriteLine($"Vous avez gagnez {mise * 2}");
                }
                else
                {
                    Console.WriteLine($"Vous avez perdu {mise}");
                }
            }
            else if  (choix == "impair" || choix == "impaire")
            {
                if (nbroulette % 2 == 0)
                {
                    Console.WriteLine($"Vous avez perdu {mise}");
                }
                else
                {
                    Console.WriteLine($"Vous avez gagnez {mise * 2}");
                }

            }

            Console.ReadKey();
        }
    }
}
