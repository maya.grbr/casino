﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
///Projet :  Casino
///Détails : Machine à sous
///Auteur : M.Souza De Farias
///Version : 1.0
///Date: 25.11.2022
/// </summary>

namespace machineASous
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ///Création des nombre aléatoires des machines
            Random imageNombreRandom = new Random();

            int image1 = imageNombreRandom.Next(1, 7);

            int image2 = imageNombreRandom.Next(1, 7);

            int image3 = imageNombreRandom.Next(1, 7);

            //Affichage des règles du jeu
            Console.WriteLine("Bonjour ! Bienvenue sur le jeu de la machine à sous !");
            Console.WriteLine("");
            Console.WriteLine("Le but de ce jeu est très basique:");
            Console.WriteLine("");
            Console.WriteLine("Vous aller miser une certaine somme et choisir un chiffre qui va de 1 à 6.");
            Console.WriteLine("La machine va lancer trois chiffres différents.");
            Console.WriteLine("Si les trois chiffres que la machine fait apparaitre sont identique à celui que vous avez choisi, c'est bingo ! Vous aurez tripler votre somme.");
            Console.WriteLine("Si seulement deux chiffres sont identique au votre, votre mise sera perdue mais vous pouvez toujours essayer de remiser pour la récupérer et peut-être même... faire bénéfice ? ;)");
            Console.WriteLine("Si aucun des chiffre n'est celui que vous avez choisi ou qu'il n'y en ai qu'un, vous avez perdu et votre mise sera donc perdue. :(");
            Console.WriteLine("");

            ///Demande de combien le joueur veut miser
            Console.Write("Veuillez entrer votre mise : ");

            ///Déclaration de la mise du joueur
            int mise = Convert.ToInt32(Console.ReadLine());

            ///Demande du chiffre sur lequel le joueur veut parier
            Console.WriteLine("");
            Console.Write("Veuillez choisir votre chiffre : ");

            ///Déclaration du chiffre sur lequel le joueur veut parier
            int chiffreDuJoueur = Convert.ToInt32(Console.ReadLine());

            ///Affichage des résultat des machines
            affichage();
            Console.WriteLine($"Le résultat de la première machine est {image1}.");
            Console.WriteLine($"Le résultat de la deuxième machine est {image2}.");
            Console.WriteLine($"Le résultat de la troisième machine est {image3}.");
            affichage();

            ///Méthode pour si le joueur est gagnant
            if (chiffreDuJoueur == image1 && chiffreDuJoueur == image2 && chiffreDuJoueur == image3)
            {
                Console.WriteLine("Bingo ! Vous avez gagné(e) le jackpot !");
                mise = mise * 3;
                Console.WriteLine($"Votre mise est maintenant de {mise}.");
                Console.WriteLine("Cela serait incroyable de gagner encore trois fois plus ! Vu votre chance je vous conseille de retanter ! ;)");
            }

            ///Méthode pour si le joueur est perdant
            if (chiffreDuJoueur != image1 && chiffreDuJoueur != image2 && chiffreDuJoueur != image3)
            {
                Console.WriteLine("Votre mise a été réduite à 0.");
                Console.WriteLine("Vous puez la mal chance. Ne revenez plus.");
            }

            ///Méthode pour si le joueur est perdant mais qu'il ne lui manque plus qu'un chiffre pour gagner
            if (chiffreDuJoueur == image1 && chiffreDuJoueur == image2 && chiffreDuJoueur != image3)
            {
                Console.WriteLine("Votre mise a donc été réduite à 0 :(");
                Console.WriteLine("Vous y étiez presque! La prochaine est peut-être être la bonne ?");
            }

            ///Méthode pour si le joueur est perdant mais qu'il ne lui manque plus qu'un chiffre pour gagner
            if (chiffreDuJoueur == image1 && chiffreDuJoueur != image2 && chiffreDuJoueur == image3)
            {
                Console.WriteLine("Votre mise a donc été réduite à 0 :(");
                Console.WriteLine("Vous y étiez presque! La prochaine est peut-être être la bonne ?");
            }

            ///Méthode pour si le joueur est perdant mais qu'il ne lui manque plus qu'un chiffre pour gagner
            if (chiffreDuJoueur != image1 && chiffreDuJoueur == image2 && chiffreDuJoueur == image3)
            {
                Console.WriteLine("Votre mise a donc été réduite à 0 :(");
                Console.WriteLine("Vous y étiez presque! La prochaine est peut-être être la bonne ?");
            }

            ///Méthode pour si le joueur est perdant mais qu'il lui manque 2 chiffre pour être gagnant
            if (chiffreDuJoueur == image1 && chiffreDuJoueur != image2 && chiffreDuJoueur != image3)
            {
                Console.WriteLine("Votre mise a été réduite à 0.");
                Console.WriteLine("Vous puez la mal chance. Ne revenez plus.");
            }

            ///Méthode pour si le joueur est perdant mais qu'il lui manque 2 chiffre pour être gagnant
            if (chiffreDuJoueur != image1 && chiffreDuJoueur == image2 && chiffreDuJoueur != image3)
            {
                Console.WriteLine("Votre mise a été réduite à 0.");
                Console.WriteLine("Vous puez la mal chance. Ne revenez plus.");
            }

            ///Méthode pour si le joueur est perdant mais qu'il lui manque 2 chiffre pour être gagnant
            if (chiffreDuJoueur != image1 && chiffreDuJoueur != image2 && chiffreDuJoueur == image3)
            {
                Console.WriteLine("Votre mise a été réduite à 0.");
                Console.WriteLine("Vous puez la mal chance. Changez de jeu.");
            }

            ///Méthode pour faire en sorte que le pannel ne s'enlève pas instantanement
            Console.ReadLine();
        }

        /// <summary>
        /// Méthode pour faire des sauts de lignes.
        /// </summary>
        private static void affichage()
        {
            Console.WriteLine("============================================================================================");
        }
    }
}
