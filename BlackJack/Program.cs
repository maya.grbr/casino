﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Lifetime;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

/* 
 *  Auteur : Yoan Bompas
 *  Projet : Faire le jeu blackjack
 *  Détail : Faire comme le vrai jeu blackjack
 */

namespace BlackJack
{
    internal class Program
    {
        static int tireAleatoire(Random rnd)
        {
            /*
             * Tirer un nombre mais avez la bonne chance de tonmber sur chaque carte
             */

            int randomValue = rnd.Next(208);
            if (randomValue >= 0 && randomValue <= 16)
            {
                return 2;
            }
            else if (randomValue > 16 && randomValue <= 33)
            {
                return 3;
            }
            else if (randomValue > 33 && randomValue <= 49)
            {
                return 4;
            }
            else if (randomValue > 49 && randomValue <= 65)
            {
                return 5;
            }
            else if (randomValue > 65 && randomValue <= 81)
            {
                return 6;
            }
            else if (randomValue > 81 && randomValue <= 97)
            {
                return 7;
            }
            else if (randomValue > 97 && randomValue <= 113)
            {
                return 8;
            }
            else if (randomValue > 113 && randomValue <= 129)
            {
                return 9;
            }
            else if (randomValue > 129 && randomValue <= 192)
            {
                return 10;
            }
            else 
            {
                return 11;
            }
        }
        static void displayLine()
        {
            Console.WriteLine("======================================================================================================");
        }
        static void displayLine2()
        {
            Console.WriteLine("-----------------------------------------");
        }
        static void regleJeu()
        {
            /*
             * Affichage des régle
             */

            displayLine();
            Console.WriteLine("Bienvenu sur le jeu du BlackJack");
            Console.WriteLine("Les régle du jeu sont les suivente:");
            Console.WriteLine(" - On vous donne deux carte aléatoire entre 2 et 10 (regarder plus bas pour l'As)");
            Console.WriteLine(" - Vous pouvez redemender autant de carte que vous voullez");
            Console.WriteLine(" - Le but est de se raprochez le plus de 21, sinon vous dépassez 21 vous avez perdu");
            Console.WriteLine(" - Si vous tirer un as sa valeur est 10 mais si votre totale dépasse 21 il se transforme en 1");
            Console.WriteLine(" - Vous joué contre le croupier avec l'ordinateur dans ce cas là");
            Console.WriteLine("    ° Le croupier a des régle a lui");
            Console.WriteLine("      - Il vas avoir deux carte aléatoire entre 2 et 10 et 11 (avec l'as)");
            Console.WriteLine("      - Il vas avoir un carte cachez et un carte visible");
            Console.WriteLine("      - Il dois retire une carte si il a 16 ou moins");
            Console.WriteLine("      - Il dois arréter de tirer une carte si il a entre 17 et 21");
            displayLine();
        }
        static void Main(string[] args)
        {
            /*
             * Affichage régle + donner le nombre aléatoire 
             */

            regleJeu();
            Random rnd = new Random();

            /*
             * Demander la mise
             */

            Console.WriteLine("Combien voulez vous miser pour cette manche");
            double mise = Convert.ToDouble(Console.ReadLine());

            displayLine2();

            /* 
             * Les tires pour le joueur 
             */

            int totalPointJoueur = 0;

            int randomValue = tireAleatoire(rnd);
            Console.WriteLine("Voici votre première carte : " + randomValue);
            totalPointJoueur = totalPointJoueur + randomValue;

            randomValue = tireAleatoire(rnd);
            Console.WriteLine("Voici votre deuxième carte : " + randomValue);
            totalPointJoueur = totalPointJoueur + randomValue;

            Console.WriteLine("Vous avez pour l'instant un score de : " + totalPointJoueur);

            displayLine2();

            /* 
             * Les tires pour le croupier
             */

            int totalPointCroupier = 0;

            randomValue = tireAleatoire(rnd);
            Console.WriteLine("Voici la première carte du croupier : " + randomValue);
            totalPointCroupier = totalPointCroupier + randomValue;

            
            Console.WriteLine("Voici la deuxième carte du croupier : Carte cachée");
            int carteCacher = tireAleatoire(rnd);
            totalPointCroupier = totalPointCroupier + carteCacher;
            
            displayLine2();

            /*
             * Savoir si le joueur veut retirer
             */

            Console.WriteLine("Voulez vous retirez une carte ? (oui ou non sans les majuscules)");
            string repRetirerCarte = Console.ReadLine();

            /*
             * Renter dans la condition si "oui"
             */

            while(repRetirerCarte == "oui")
            {
                randomValue = tireAleatoire(rnd);
                Console.WriteLine("Voici votre quatrième : " + randomValue);
                totalPointJoueur = totalPointJoueur + randomValue;

                Console.WriteLine("Vous avez pour l'insatant un score de : " + totalPointJoueur);
                SiPlus21(totalPointJoueur,mise);
                SiEtegale21(totalPointJoueur,mise);
                displayLine2();

                Console.WriteLine("Voulez vous retirez une carte ? (oui ou non sans les majuscules)");
                repRetirerCarte = Console.ReadLine();
            }

            /*
             * Renter dans la condition si "non"
             */

            if (repRetirerCarte == "non")
            {
                Console.WriteLine($"La carte chaché du croupier était un {carteCacher} il a donc pour l'instant un score de : " + totalPointCroupier);
                
                /*
                 * Si il a 21 il a perdu directement 
                 */ 


                Console.WriteLine("Vous vous avez un score de : " + totalPointJoueur);

                /*
                 * Tans que le croupier a moin ou 16 il dois retirer une carte
                 */
                while(totalPointCroupier <= 16)
                {
                    randomValue = tireAleatoire(rnd);
                    Console.WriteLine("Voici tire du croupier : " + randomValue);
                    totalPointCroupier = totalPointCroupier + randomValue;
                    Console.WriteLine("Il a donc : " + totalPointCroupier);
                }
                if (totalPointCroupier > 21)
                {
                    Console.WriteLine("Le croupier a perdu");
                    mise *= 2;
                    Console.WriteLine("Tapez n'importe quelle touche pour quitter");
                    Console.ReadKey();
                    return;
                }
            }

            /*
             * Les condition pour gagner
             */

            if (totalPointJoueur > totalPointCroupier) // si il a plus que le croupier
            {
                Console.WriteLine("Vous avez gagner 2 fois votre mise soit : " + mise * 2);
                mise = mise * 2;
                Console.WriteLine("Tapez n'importe quelle touche pour quitter");
                Console.ReadKey();
                return;
            }
            else if (totalPointJoueur < totalPointCroupier) // si il a moins que le croupier
            {
                Console.WriteLine("Le croupier a gagner. Vous avez perdu votre mise soit : " + mise);
                mise =0;
                Console.WriteLine("Tapez n'importe quelle touche pour quitter");
                Console.ReadKey();
                return;
            }

            Console.ReadKey();
        }

        static void SiEtegale21(int totalPointJoueur, double mise)
        {
            if (totalPointJoueur == 21)
            {
                Console.WriteLine("Vous avez gagner 3 fois votre mise soit : " + mise * 3);
                mise = mise * 3;
                Console.WriteLine("Tapez n'importe quelle touche pour quitter");
                Console.ReadKey();
                Environment.Exit(0);
            }
        }

        static void SiPlus21(int totalPointJoueur, double mise)
        {
            if (totalPointJoueur > 21)
            {
                Console.WriteLine("Vous avez perdu : " + mise);
                mise = 0; //TODO mettre à jour le fichier du score
                Console.WriteLine("Tapez n'importe quelle touche pour quitter");
                Console.ReadKey();
                Environment.Exit(0);
            }
        }
    }
}
