﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Projet : casino, restaurant 
 * Détails : restaurant, faites une pause et choisissez ce que vous voulez manger
 * Auteur : Maya Gerber
 * Date : 25.11.2022
 */
namespace foodTruck
{
    internal class Program
    {
        /// <summary>
        /// Vous êtes au restaurant et vous avez le choix entre du salé, sucré et des boissons. 
        /// Faites votre commande !
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            double totalSale = 0;
            double totalSucree = 0;
            double totalBoisson = 0;
            double total = 0;
            ligne();
            Console.WriteLine("Bienvenue au restaurant !");
            ligne();
            string queVoulezVousManger = "";
            while (queVoulezVousManger != "rien")
            {
                Console.WriteLine("Voulez-vous manger (a) salé, (b) sucré, (c) une boisson ou c'est tout ? (a/b/c/c'est tout) ");
                queVoulezVousManger = Console.ReadLine();

                if (queVoulezVousManger == "a")
                {
                    totalSale = sale();
                }
                else if (queVoulezVousManger == "b")
                {
                    totalSucree = sucre();
                }
                else if (queVoulezVousManger == "c")
                {
                    totalBoisson = boissons();
                }
                else
                {
                    total = totalSale + totalSucree + totalBoisson;
                    Console.WriteLine($"La somme est de {total}fr.");
                    Console.WriteLine("Merci et à bientôt !!");
                    
                }
            }



        }
        /// <summary>
        /// la fonction des plats salés
        /// </summary>
        static double sale()
        {
            int queVoulezVousManger = 0;
            int combienDePortion;
            const double PIZZA = 7.0;
            const double BURGERSALADE = 5.0;
            const double CHEESEBURGER = 6.0;
            const double SANDWITCH = 3.0;

            const double PETITEFRITES = 2.5;
            const double GRANDEFRITES = 3.5;
            double totalSale = 0;
            ligne();
            Console.WriteLine("Voici la carte salé :");
            ligne();
            Console.WriteLine("1) c'est tout");
            Console.WriteLine("2) Pizza margarita [7.0fr]");
            Console.WriteLine("3) Pizza Pepperoni [7.0fr]");
            Console.WriteLine("4) Pizza champinions [7.0fr]");
            Console.WriteLine("5) Burger [5.0fr]");
            Console.WriteLine("6) Cheese-Bruger [6.0fr]");
            Console.WriteLine("7) Sandwitch jambon [3.0fr]");
            Console.WriteLine("8) Sadwitch salami [3.0fr]");
            Console.WriteLine("9) Salade césard [5.0fr]");
            Console.WriteLine("10) Salade nature [5.0fr]");
            Console.WriteLine("11) Petite portion de frites [2.50fr]");
            Console.WriteLine("12) Grand portion de frites [3.50fr]");
            ligne();
            while (queVoulezVousManger != 1)
            {
                Console.WriteLine("Que souhaitez-vous manger ? (1,2,3,4,5,6,7,8,9,10,11 ou 12)");
                queVoulezVousManger = Convert.ToInt32(Console.ReadLine());
               




                if (queVoulezVousManger == 2 || queVoulezVousManger == 3 || queVoulezVousManger == 4)
                {
                    Console.WriteLine("Combien de fois en voulez-vous ?");
                    combienDePortion = Convert.ToInt32(Console.ReadLine());
                    totalSale = totalSale + combienDePortion * PIZZA;
                }
                else if (queVoulezVousManger == 5 || queVoulezVousManger == 9 || queVoulezVousManger == 10)
                {
                    Console.WriteLine("Combien de fois en voulez-vous ?");
                    combienDePortion = Convert.ToInt32(Console.ReadLine());
                    totalSale = totalSale + combienDePortion * BURGERSALADE;
                }
                else if (queVoulezVousManger == 6)
                {
                    Console.WriteLine("Combien de fois en voulez-vous ?");
                    combienDePortion = Convert.ToInt32(Console.ReadLine());
                    totalSale = totalSale + combienDePortion * CHEESEBURGER;
                }
                else if (queVoulezVousManger == 7 || queVoulezVousManger == 8)
                {
                    Console.WriteLine("Combien de fois en voulez-vous ?");
                    combienDePortion = Convert.ToInt32(Console.ReadLine());
                    totalSale = totalSale + combienDePortion * SANDWITCH;
                }
                else if (queVoulezVousManger == 11)
                {
                    Console.WriteLine("Combien de fois en voulez-vous ?");
                    combienDePortion = Convert.ToInt32(Console.ReadLine());
                    totalSale = totalSale + combienDePortion * PETITEFRITES;
                }
                else if (queVoulezVousManger == 12)
                {
                    Console.WriteLine("Combien de fois en voulez-vous ?");
                    combienDePortion = Convert.ToInt32(Console.ReadLine());
                    totalSale = totalSale + combienDePortion * GRANDEFRITES;

                }

                Console.WriteLine($"Le prix total est de {totalSale} fr.");
                ligne();

            }
            return totalSale;


        }
        /// <summary>
        /// ligne 
        /// </summary>
        static void ligne()
        {
            Console.WriteLine("---------------------------");
        }
        /// <summary>
        /// la fonction pour les boissons
        /// </summary>
        static double boissons()
        {
            int queVoulezVousManger = 0;
            int combienDePortion;
            double totalBoisson = 0;
            const double EAU = 1.0;
            const double COCAFANTAJUSSPRITE = 1.20;
            const double SODA = 5.0;

            ligne();
            Console.WriteLine("Voici la carte des boissons :");
            ligne();
            Console.WriteLine("1) c'est tout");
            Console.WriteLine("2) Eau [1.0fr]");
            Console.WriteLine("3) Coca-cola [1.20fr]");
            Console.WriteLine("4) Fanta [1.20fr]");
            Console.WriteLine("5) Jus de pomme [1.20fr]");
            Console.WriteLine("6) Soda à la banane [5.0fr]");
            Console.WriteLine("7) Sprite [1.20]");
            ligne();
            while (queVoulezVousManger != 1)
            {
                Console.WriteLine("Que souhaitez-vous boire ? (1,2,3,4,5,6 ou 7)");
                queVoulezVousManger = Convert.ToInt32(Console.ReadLine());
                
                if (queVoulezVousManger == 2)
                {
                    Console.WriteLine("Combien de fois en voulez-vous ?");
                    combienDePortion = Convert.ToInt32(Console.ReadLine());
                    totalBoisson = totalBoisson + combienDePortion * EAU;
                }
                else if (queVoulezVousManger == 3 || queVoulezVousManger == 4 || queVoulezVousManger == 5 || queVoulezVousManger == 7)
                {
                    Console.WriteLine("Combien de fois en voulez-vous ?");
                    combienDePortion = Convert.ToInt32(Console.ReadLine());
                    totalBoisson = totalBoisson + combienDePortion * COCAFANTAJUSSPRITE;
                }
                else if (queVoulezVousManger == 6)
                {
                    Console.WriteLine("Combien de fois en voulez-vous ?");
                    combienDePortion = Convert.ToInt32(Console.ReadLine());
                    totalBoisson = totalBoisson + combienDePortion * SODA;
                }
                Console.WriteLine($"Le prix total est de {totalBoisson} fr.");
                ligne();
            }
            return totalBoisson;


        }
        /// <summary>
        /// la fonction pour les plats sucrés
        /// </summary>
        static double sucre()
        {
            int queVoulezVousManger = 0;
            int combienDePortion;
            const double DONUT = 2.50;
            const double TARTE = 4.0;
            const double MACARONCOOKIE = 2.0;
            const double POPCORNE = 3.0;
            double totalSucree = 0;

            ligne();
            Console.WriteLine("Voici la carte sucré : ");
            ligne();

            Console.WriteLine("1) c'est tout");
            Console.WriteLine("2) Donut vanille [2.50fr]");
            Console.WriteLine("3) Donut chocolat [2.50fr]");
            Console.WriteLine("4) Tarte aux fraises [4.0fr]");
            Console.WriteLine("5) Tarte aux pommes [4.0fr]");
            Console.WriteLine("6) Macaron fraise [2.0fr]");
            Console.WriteLine("7) Macaron chocolat [2.0fr]");
            Console.WriteLine("8) Macaron vanille [2,0fr]");
            Console.WriteLine("9) Cookie chocolat [2.0fr]");
            Console.WriteLine("10) Popcornes caramélisés [3.0fr]");
            ligne();
            while (queVoulezVousManger != 1)
            {
                
                Console.WriteLine("Que souhaitez-vous manger ? (1,2,3,4,5,6,7,8,9 ou 10)");
                queVoulezVousManger = Convert.ToInt32(Console.ReadLine());

               
                if (queVoulezVousManger == 2 || queVoulezVousManger == 3)
                {
                    Console.WriteLine("Combien de fois en voulez-vous ?");
                    combienDePortion = Convert.ToInt32(Console.ReadLine());
                    totalSucree = totalSucree + combienDePortion * DONUT;
                }
                else if (queVoulezVousManger == 4 || queVoulezVousManger == 5)
                {
                    Console.WriteLine("Combien de fois en voulez-vous ?");
                    combienDePortion = Convert.ToInt32(Console.ReadLine());
                    totalSucree = totalSucree + combienDePortion * TARTE;
                }
                else if (queVoulezVousManger == 6 || queVoulezVousManger == 7 || queVoulezVousManger == 8 || queVoulezVousManger == 9)
                {
                    Console.WriteLine("Combien de fois en voulez-vous ?");
                    combienDePortion = Convert.ToInt32(Console.ReadLine());
                    totalSucree = totalSucree + combienDePortion * MACARONCOOKIE;
                }
                else if (queVoulezVousManger == 3)
                {
                    Console.WriteLine("Combien de fois en voulez-vous ?");
                    combienDePortion = Convert.ToInt32(Console.ReadLine());
                    totalSucree = totalSucree + combienDePortion * POPCORNE;
                }
                Console.WriteLine($"Le prix total est de {totalSucree} fr.");
                ligne();

            }
            return totalSucree;




        }
    }
}

