﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Projet : Bataille
 * Details : jeux de 2 joueurs, chaques joueurs tirent 1 carte chacun. La personne qui a la carte la plus grande remporte les deux cartes. A la fin du tas le joueur qui a le plus de cartes gagne
 * Auteur : K. Sauser - CFPT - 2022
 * Date : 25.11.2022
 * Classe : IDA_P1B
 */

namespace Bataille
{
    internal class Program
    {
        static void regleDeJeux()
        {
            Console.WriteLine("> Ce jeux se joue avec 2 joueurs");
            Console.WriteLine("> Chaque joueurs ont 6 cartes ");
            Console.WriteLine("> Les deux joueurs vont retounrer 1 carte et celui qui ala carte la plus grande le remporte (donc prend les deux cartes)");
            Console.WriteLine("> À la fin du tas, l'ordinateur comptera combien de cartes chaque joueurs en ont et celui qui a le plus de cartes gagne");
        }

        static void espacement()
        {
            Console.WriteLine("---------------------------------------------------------------------------------------------------");
        }

        static void Main(string[] args)
        {

            Random random = new Random();

            Console.WriteLine("Bienvenu au jeux de la bataille ");
            Console.WriteLine(" O ");
            Console.WriteLine("-|-");
            Console.WriteLine("/|");
            Console.WriteLine("");

            espacement();

            regleDeJeux();

            espacement();
            Console.Write("Combien voulez-vous miser ? ");
            int mise = Convert.ToInt32(Console.ReadLine());

            espacement();
            Console.Write("Combien de mini bataille voulez-vous faire ? ");
            int nbBataille = Convert.ToInt32(Console.ReadLine());
            espacement();

            int i = 0;


            int joueur1 = 0;
            int joueur2 = 0;

            while (i < nbBataille)
            {
                int carteAleatoire = random.Next(0, 13);
                int carteAleatoire2 = random.Next(0, 13);

                Console.WriteLine($"votre carte est : {carteAleatoire}");
                Console.WriteLine($"La carte de l'adversaire est : {carteAleatoire2}");

                if (carteAleatoire > carteAleatoire2)
                {
                    joueur1 = joueur1 + 2;
                }

                if (carteAleatoire < carteAleatoire2)
                {
                    joueur2 = joueur2 + 2;

                }

                else if (carteAleatoire == carteAleatoire2)
                {
                    Console.WriteLine($"votre carte est : {carteAleatoire}");
                    Console.WriteLine($"La carte de l'adversaire est : {carteAleatoire2}");

                    if (carteAleatoire > carteAleatoire2)
                    {
                        joueur1 = joueur1 + 2;
                    }

                    if (carteAleatoire < carteAleatoire2)
                    {
                        joueur2 = joueur2 + 2;

                    }
                }

                i++;
            }

            espacement();
            Console.WriteLine("Vous avez : " + joueur1 + " cartes");
            Console.WriteLine("Votre adversaire a : " + joueur2 + " cartes");
            espacement();

            if (joueur1 > joueur2)
            {
                mise = mise * 20 / 100;
                Console.WriteLine("Vous avez gagné " + mise + "CHF");
            }

            else if (joueur1 < joueur2)
            {
                mise = mise - 20 / 100;
                Console.WriteLine("Vous avez perdu " + mise + "CHF");
            }

            Console.ReadKey();
        }
    }
}
